package equipos;


import tekgenesis.form.Action;

/** User class for Form: EquiposForm */
public class EquiposForm
    extends EquiposFormBase
{

    @Override
    public Action create(){

        final Equipos equipos = Equipos.create();
        copyTo(equipos);
        equipos.insert();
        setId(equipos.getId());
        return actions.navigate(EquiposFormList.class);



    }


}
