package equipos;

import org.jetbrains.annotations.NotNull;
import tekgenesis.form.Action;
import static tekgenesis.database.Databases.commitTransaction;
import tekgenesis.form.Action;

/** User class for Form: EquiposFormList */
public class EquiposFormList extends EquiposFormListBase {

    //~ Inner Classes ............................................................................................................

    public Action modEquipo() {
        // selecciono por id wacheeen
        final Integer id = getEquipos().getCurrent().getId();
        // redirige a la url del form mas la id, para poder modificarlo
        // onda, si vos en la url pones a mano http://suigen-vm02:8080/#form/equipos.EquiposForm/3
        // vas a editar el equipo con id = 3, si esto no esta con seguridad GG papu...
        // el leave no se bien que carajos hace, porque revise la class
        // y mucho no hay, magia?
        return actions.navigate(EquiposForm.class, id + "").leave();
    }

    public Action eliminarEquipo() {
        final Integer id = getEquipos().getCurrent().getId();
        getEquipos().removeCurrent(); // lo elimina de la vista, pero no persiste ese cambio
        eliminarEquipoBD(id); // lo elimino de la bd y persiste el cambio
        return actions.getDefault(); // quedo en la misma vista
    }

    public Action agregarEquipo() {
        return actions.navigate(EquiposForm.class);
    }

    private void eliminarEquipoBD(Integer id) {
        final Equipos equipo = Equipos.find(id);
        equipo.delete();
        commitTransaction();
    }


    
    public static class EquiposRow extends EquiposRowBase {
        @Override public void populate(@NotNull Equipos equipos) {
            super.populate(equipos);
        }
    }
}
