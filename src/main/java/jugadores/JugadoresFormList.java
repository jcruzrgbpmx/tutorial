package jugadores;

import org.jetbrains.annotations.NotNull;
import tekgenesis.form.Action;
import static tekgenesis.database.Databases.commitTransaction;

/** User class for Form: JugadoresFormList */
public class JugadoresFormList extends JugadoresFormListBase {


    public Action agregarJugador() {
        return actions.navigate(JugadoresForm.class);
    }

    public Action modJugador() {
        final Integer id = getJugadores().getCurrent().getId();
        return actions.navigate(JugadoresForm.class, id + "");
    }

    public Action eliminarJugador() {
        final Integer id = getJugadores().getCurrent().getId();
        getJugadores().removeCurrent(); // lo elimina de la vista, pero no persiste ese cambio
        eliminarJugadorBD(id); // lo elimino de la bd y persiste el cambio
        return actions.getDefault(); // quedo en la misma vista
    }

    private void eliminarJugadorBD(Integer id) {
        final Jugadores jugador = Jugadores.find(id);
        jugador.delete();
        commitTransaction();
    }

    // Si o si hay que crear esta class static que extienda de JugadoresRowBase
    // que se encuentra dentro de la clase base JugadoresFormListBase
    // y lo peor... es que no se porque??? Magia...
    public static class JugadoresRow extends JugadoresRowBase {
        @Override public void populate(@NotNull Jugadores jugadores) {
            super.populate(jugadores);
        }
    }
}
