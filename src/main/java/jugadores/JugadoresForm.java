package jugadores;

import equipos.EquiposForm;
import org.jetbrains.annotations.NotNull;
import tekgenesis.form.Action;

/** User class for Form: JugadoresForm */
public class JugadoresForm extends JugadoresFormBase {

    @NotNull @Override
    public Action create() {
        // despues de crear, redirige a la lista de jugadores
        final Jugadores jugadores = Jugadores.create();
        copyTo(jugadores);
        jugadores.insert();
        setId(jugadores.getId());
        return actions.navigate(JugadoresFormList.class);
    }

    public Action agregarEquipo() {
        return actions.navigate(EquiposForm.class).leave();
    }
}
