package jugadores;

import equipos.Equipos;

form JugadoresForm "Agregar Jugadores"

    entity Jugadores {

    header {
        message(title);
    };

    id, internal, optional;
    nomJugador; // input
    nomEquipo; // input
    agregar "Agregar equipo" : button, content_style "btn-success", icon plus, on_click agregarEquipo; // columna modificar

    footer {
        button(save), content_style "btn-success";
        button(cancel), content_style "btn-danger";
    };
}

form JugadoresFormList "Lista de jugadores" {

    header {
        message(title);
    };

    // tabla de jugadores, el parametro 20 es el maximo de filas a mostrar
    // el on_load "nombreFuncion" te genera esa funcion en la clase base
    // de la entidad que vas a usar para la tabla
    jugadores : Jugadores, table(20), on_load loadJugadores {
        id, internal, optional;
        nomJugador, display; // columna de la tabla
        nomEquipo, display; // columna de la tabla
        modificar "Modificar" : button, content_style "btn-info", icon pencil, on_click modJugador; // columna modificar
        eliminar "Eliminar" : button, content_style "btn-danger", icon remove, on_click eliminarJugador; // columna eliminar
    };

    footer {
        agregar "Agregar" : button, content_style "btn-success", icon plus, on_click agregarJugador;
    };
}

form JugList listing Jugadores;