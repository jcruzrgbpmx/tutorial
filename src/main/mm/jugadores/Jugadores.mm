package jugadores schema futbol;

import equipos.Equipos;

entity Jugadores searchable deprecable {

    nomJugador "Nombre Jugador" : String(20);
    nomEquipo "Equipo" : Equipos;
}
