package equipos;

form EquiposForm "Equipos"

    entity Equipos {

    header {
        message(title);
    };

    id, internal, optional;
    nomEquipo; // campo input

    footer {
        button(save);
        button(cancel);
    };
}

form EquiposFormList "Lista de equipos" {

    header {
        message(title);
        agregar "Agregar Equipo" : button, icon plus, on_click agregarEquipo; // columna eliminar

    };

    // se van a mostrar 20 registros por paginacion
    equipos : Equipos, table(20), on_load loadEquipos {
        id, internal, optional;
        nomEquipo, display; // columna de la tabla
        modificar "Modificar" : button, icon pencil, on_click modEquipo; // columna modificar
        eliminar "Eliminar" : button, icon remove, on_click eliminarEquipo; // columna eliminar
    };

}

